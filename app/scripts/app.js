'use strict';

/**
 * @ngdoc overview
 * @name usersApp
 * @description
 * # usersApp
 *
 * Main module of the application.
 */
 angular
  .module('usersApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      }).when('/user/:userName/:userSurname/:userId', {
        templateUrl: 'views/user.html',
        controller: 'UserCtrl'
      }).when('/user/', {
        templateUrl: 'views/user.html',
        controller: 'UserCtrl'
      }).when('/projects/', {
        templateUrl: 'views/projects.html',
        controller: 'ProjectsCtrl'
      }).when('/projects/:projectName/:projectId', {
        templateUrl: 'views/project.html',
        controller: 'ProjectCtrl'
      }).when('/projects/:projectId', {
        templateUrl: 'views/project.html',
        controller: 'ProjectCtrl'
      }).when('/role/:roleId/', {
        templateUrl: 'views/role.html',
        controller: 'roleCtrl'
      }).when('/role/', {
        templateUrl: 'views/role.html',
        controller: 'roleCtrl'
      }).otherwise({
        redirectTo: '/'
      });
  });
