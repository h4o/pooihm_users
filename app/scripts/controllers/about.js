'use strict';

/**
 * @ngdoc function
 * @name usersApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the usersApp
 */
angular.module('usersApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
