'use strict';

/**
 * @ngdoc function
 * @name usersApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the usersApp
 */
angular.module('usersApp')
  .controller('MainCtrl',['$scope','$http','UsersService',function ($scope,$http,UsersService) {
  	$scope.getUsers = function(){
      UsersService.getUsers(function(data){
        $scope.users = data;
      })
    }
    $scope.getUsers();
  }]);
