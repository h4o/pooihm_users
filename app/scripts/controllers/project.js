'use strict';

/**
 * @ngdoc function
 * @name ProjectApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ProjectApp
 */
angular.module('usersApp')
  .controller('ProjectCtrl',['$scope','$http','$routeParams','$location','ProjectsService',function ($scope,$http,$routeParams,$location,ProjectsService) {

  	$scope.getProject = function(id){
      ProjectsService.getProject(id,function(data){
        $scope.project = data;
      })
    };
    $scope.getProjectRoles = function(id){
      ProjectsService.getProjectRoles(id,function(data){
        $scope.roles = data;
      })
    };
    $scope.getProjectUsers = function(id){
      ProjectsService.getProjectUsers(id,function(data){
        $scope.users = data;
      })
    };
    $scope.getProjectRoles =  function(id){
      ProjectsService.getProjectRoles(id,function(data){
        $scope.roles = data;
      })
    };
  $scope.deleteUserRoleProject = function(userId){
        ProjectsService.deleteProjectUsers($routeParams.projectId,roleId, function(data){
      $scope.getProjectRoles($routeParams.projectId);
    });
      };
  $scope.deleteRole = function (roleId){
    ProjectsService.deleteRole(roleId, function(data){
      $scope.getProjectRoles($routeParams.projectId);
    });
  };
  $scope.toggleRoleEdit = function(role){
    role.editmode  = !role.editmode;
    for(var i = 0; i < $scope.roles.length; i++){
      if($scope.roles[i].id == role.id){
        $scope.roles[i].id = role.editmode;
      }
    }
  }
	$scope.toggleEdit = function(){
		if($scope.editmode){
      if($scope.createmode){
        ProjectsService.addProject($scope.project, function(data){
          $scope.project = data;
          $location.path('/project/'+$scope.project.name+'/'+$scope.id);
        });
      } else {
        ProjectsService.editProject($scope.project, function(data){
          $scope.project = data;
        });
      }
			//traitement spécifique
      if(!$scope.createmode)
			$scope.editmode = false;
      $scope.buttonText = 'editer';
		}
    else{
      $scope.editmode = true;
      $scope.buttonText = 'enregistrer';
    }
    console.log($scope.editmode);
	};
  $scope.cancel = function() {
     $scope.getProject($routeParams.projectId);
     $scope.editmode = false;
  }
  $scope.delete = function() {
    ProjectsService.deleteProject($routeParams.projectId);
    $location.path("/projects");
  }
  if($routeParams.projectId){
      $scope.createmode = false;
      $scope.editmode=false;
      $scope.buttonText = 'editer';
      $scope.getProject($routeParams.projectId);
      $scope.getProjectUsers($routeParams.projectId);
      $scope.getProjectRoles($routeParams.projectId);
      
  } else {
      $scope.createmode = true;
      $scope.editmode=true;
      $scope.buttonText = 'enregistrer';
  }
  }]);

angular.filter('getByProperty', function() {
    return function(propertyName, propertyValue, collection) {
        var i=0, len=collection.length;
        for (; i<len; i++) {
            if (collection[i][propertyName] == +propertyValue) {
                return collection[i];
            }
        }
        return null;
    }
});
