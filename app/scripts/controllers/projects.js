'use strict';

/**
 * @ngdoc function
 * @name usersApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the usersApp
 */
angular.module('usersApp')
  .controller('ProjectsCtrl',['$scope','$http','ProjectsService',function ($scope,$http,ProjectsService) {
  	$scope.getProjects = function(){
      ProjectsService.getProjects(function(data){
        $scope.projects = data;
      })
    }
    $scope.getProjects();
  }]);
