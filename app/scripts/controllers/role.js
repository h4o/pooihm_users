'use strict';

/**
 * @ngdoc function
 * @name usersApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the usersApp
 */
angular.module('usersApp')
  .controller('roleCtrl',['$scope','$http','$routeParams','ProjectsService','UsersService',function ($scope,$http,$routeParams,ProjectsService,UsersService) {
  	$scope.getUsers = function(){
      UsersService.getUsers(function(data){
        $scope.users = data;
      })
    }
    $scope.getProjects = function(){
      ProjectsService.getProjects(function(data){
        $scope.projects = data;
      })
    }
    $scope.createRole = function(role, currentUser, currentProject){
    	if(currentUser && currentProject){
        	role.UserId = currentUser.id;
        	role.ProjectId = currentProject.id;
        	if($scope.createmode){
    		ProjectsService.addRole(role, function(){

    		});
    		}
    	}
    	else {
    			role.id = $routeParams.roleId;
    			ProjectsService.editRole(role,$routeParams.roleId,function(){
    				console.log("edited");
    			});
    		
    	}
    };
    $scope.loadRole = function(roleid){
    	ProjectsService.getRole(roleid,function(data){
    		$scope.role = data;
    		console.log(data);
    	});
    };
  	if($routeParams.roleId){
  		$scope.createmode = false;
  		$scope.loadRole($routeParams.roleId);

  	} else {
  		$scope.role = {};
  		$scope.createmode = true;
    	$scope.getUsers();
    	$scope.getProjects();
  	}

  }]);
