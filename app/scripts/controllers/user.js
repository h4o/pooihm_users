'use strict';

/**
 * @ngdoc function
 * @name usersApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the usersApp
 */
angular.module('usersApp')
  .controller('UserCtrl',['$scope','$http','$routeParams','$location','UsersService',function ($scope,$http,$routeParams,$location,UsersService) {
    
  	 $scope.getUserRole = function(id){
      UsersService.getUserRole(id,function(data){
        $scope.roles = data;
      })
    };
  	$scope.getUser = function(id){
      UsersService.getUser(id,function(data){
        $scope.user = data;
      })
    };
	$scope.toggleEdit = function(){
		if($scope.editmode){
      if($scope.createmode){
        UsersService.addUser($scope.user, function(data){
          $scope.user = data;
          $location.path('/user/'+$scope.user.name+'/'+$scope.user.surname+'/'+$scope.id);
        });
      } else {
        UsersService.editUser($scope.user, function(data){
          $scope.user = data;
        });
      }
			//traitement spécifique
      if(!$scope.createmode)
			$scope.editmode = false;
      $scope.buttonText = 'editer';
		}
    else{
      $scope.editmode = true;
      $scope.buttonText = 'enregistrer';
    }
    console.log($scope.editmode);
	};
  $scope.cancel = function() {
     $scope.getUser($routeParams.userId);
     $scope.editmode = false;
  }
  $scope.delete = function() {
    UsersService.deleteUser($routeParams.userId);
    $location.path("/");
  }
  if($routeParams.userId){
      $scope.createmode = false;
      $scope.editmode=false;
      $scope.buttonText = 'editer';
      $scope.getUser($routeParams.userId);
      $scope.getUserRole($routeParams.userId);
  } else {
      $scope.createmode = true;
      $scope.editmode=true;
      $scope.buttonText = 'enregistrer';
  }
  }]);
