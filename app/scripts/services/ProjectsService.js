angular.module('usersApp').service('ProjectsService', ['$http', function ProjectsService($http){
  var baseaddr = 'http://poo-ihm-2015-rest.herokuapp.com/api/';
  this.getProjects = function(successCB){
      $http.get(baseaddr + 'Projects').success(function(data) {
        successCB(data.data);
      });
  }

  this.getProject = function(id,successCB){
      $http.get(baseaddr + 'Projects/'+id).success(function(data) {
        successCB(data.data);
      });
  };

    this.addProject = function(userData,successCB) {
    $http.post(baseaddr+'Projects',userData).success(function(data){
      successCB(data.data);
    });
  };

    this.addRole = function(userData,successCB) {
    $http.post(baseaddr+'Roles',userData).success(function(data){
      successCB(data.data);
    });
  };
  this.editRole = function(userData,roleId,successCB) {
    $http.put(baseaddr+'Roles/'+roleId,userData).success(function(data){
      successCB(data.data);
    });
  };
    this.editProject = function(projectData,successCB) {
    $http.put(baseaddr+'Projects/'+projectData.id,projectData).success(function(data){
      successCB(data.data);
    });
  };
    this.deleteProject = function(projectId){
          $http.delete(baseaddr+'Projects/'+projectId);
    };
    this.deleteUserProject =  function(projectId,userId,successCB){
      $http.delete(baseaddr+'Projects/'+projectId+'/Users/'+userId).success(function(data){
        if(data.status === "success")
          successCB();
      })};

      this.getProjectUsers = function(projectId,successCB){
      $http.get(baseaddr + 'Projects/'+projectId+'/Users/').success(function(data) {
        successCB(data.data);
      })
      };
      this.getProjectRoles = function(projectId,successCB){
      $http.get(baseaddr + 'Projects/'+projectId+'/Roles/').success(function(data) {
        successCB(data.data);
      })
      };
    this.getRole = function(roleId,successCB){
      $http.get(baseaddr + 'Roles/'+roleId).success(function(data) {
        successCB(data.data);
      })
      };
      this.deleteRole =  function(roleId,successCB){
      $http.delete(baseaddr+'Roles/'+roleId).success(function(data){
        if(data.status === "success")
          successCB(data);
      })};
}]);