angular.module('usersApp').service('UsersService', ['$http', function UsersService($http){
  var baseaddr = 'http://poo-ihm-2015-rest.herokuapp.com/api/';
  this.getUsers = function(successCB){
      $http.get(baseaddr + 'Users').success(function(data) {
        successCB(data.data);
      });
  }

  this.getUser = function(id,successCB){
      $http.get(baseaddr + 'Users/'+id).success(function(data) {
        successCB(data.data);
      });
  };

  this.getUserRole = function(id,successCB){
    $http.get(baseaddr+'Users/'+id+'/Roles').success(function(data){
      successCB(data.data);
    });
  }
    this.addUser = function(userData,successCB) {
    $http.post(baseaddr+'Users',userData).success(function(data){
      successCB(data.data);
    });
  };
    this.editUser = function(userData,successCB) {
    $http.put(baseaddr+'Users/'+userData.id,userData).success(function(data){
      successCB(data.data);
    });
  };
    this.deleteUser = function(userId){
          $http.delete(baseaddr+'Users/'+userId);
    };

}]);